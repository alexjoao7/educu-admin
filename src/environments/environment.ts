// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,  
  user_find_by_id:'http://localhost:9090/usuarios/',
  user_create:'http://localhost:9090/usuarios/create',
  firebaseConfig:{
    apiKey: "AIzaSyAnDT-UC3o-Zhn3b68MbuCjGjGV94dWRaE",
    authDomain: "uduku-2307e.firebaseapp.com",
    databaseURL: "https://uduku-2307e.firebaseio.com",
    projectId: "uduku-2307e",
    storageBucket: "uduku-2307e.appspot.com",
    messagingSenderId: "100496435074"
  }
};
