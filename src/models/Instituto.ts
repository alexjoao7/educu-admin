export class Instituto {
    id: string;
    tipe_education: string;
    tipe_institution: string;
    nombre: string;
    imagen: string;
    mision: string;
    vision: string;
    fundado: string;
    telefono: string;
    celular: string;
    estado: string;
    provincia: string;
    ciudad: string;
    sector: string;
    direccion: string;
    observacion: string;

    constructor() { }

    Instituto(id: string, tipe_education: string, tipe_institution: string, nombre: string,
        imagen: string, mision: string, vision: string, fundado: string, telefono: string, celular: string,
        estado: string, provincia: string, ciudad: string, sector: string, direccion: string, observacion: string) {
        this.id = id;
        this.tipe_education = tipe_education;
        this.tipe_institution = tipe_institution;
        this.nombre = nombre;
        this.imagen = imagen;
        this.mision = mision;
        this.vision = vision;
        this.fundado = fundado;
        this.telefono = telefono;
        this.celular = celular;
        this.estado = estado;
        this.provincia = provincia;
        this.ciudad = ciudad;
        this.sector = sector;
        this.direccion = direccion;
        this.observacion = observacion;
    }
}