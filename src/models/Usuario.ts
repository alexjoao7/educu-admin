export class Usuario {
    ed__usua_id: number;
    usua_tipoiden: string;
    usua_iden: string;
    usua_nomb: string;
    usua_apell: string;
    usua_email: string;
    usua_password: string;
    usua_celu: string;
    usua_imagen: string;
    usua_fechnaci : Date;
    usua_genero : string;
    usua_prov: string;
    usua_naci: string;
    usua_lugnaci: string;
    usua_civil: string;
    usua_niveeduc: string;
    usua_diredomi: string;
    usua_teledomi: string;
    usua_profe: string;
    usua_ocup: string;
    usua_emprtrab: string;
    usua_diretrab: string;
    usua_teletrab: string;
    usua_vivienda: string;
    usua_estado: string;
    usua_creacion: Date;

    Usuario() { }

}