export class Propiedades {
    id: string;
    nombre: string;
    valor: string;
    observacion: string;
    estado: string;

    constructor() { }

    Propiedades(id: string, nombre: string, valor: string, observacion: string,estado: string) {
        this.id = id;
        this.nombre = nombre;
        this.valor = valor;
        this.observacion = observacion;
        this.estado = estado;
    }
}