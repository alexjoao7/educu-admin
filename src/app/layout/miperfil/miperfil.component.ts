import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { RequestOptions, Headers } from '@angular/http';
import { HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Usuario } from '../../../models/Usuario';

import { HttpService } from '../../services/http.service';

export interface Familiar {
  cedula: String;
  nombres: String;
  apellidos: String;
  genero: String;
  fechnacimineto: String;
  correo: String;
  celular: String;
  provedor: String;
  foto: String;
}

@Component({
  selector: 'app-miperfil',
  templateUrl: './miperfil.component.html',
  styleUrls: ['./miperfil.component.scss'],
  animations: [routerTransition()]
})
export class MiperfilComponent implements OnInit {

  public usuario = new Usuario();
  public elements = false;
  date2 = Date;
  url: String = "";
  familiar: Familiar;
  prueba: String = "";

  private enform: Boolean = true;
  public btnSave: Boolean = false;
  public btnEdit: Boolean = true;

  constructor(public http: HttpService) { }

  ngOnInit() {
    this.url = "assets/images/usuario.png";
    //this.getDataUsuario();
  }


  async getDataUsuario() {
    let myHeaders = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    var httpOptions = {
      headers: myHeaders
    };
    this.http.getById(environment.user_find_by_id, '6', httpOptions)
      .subscribe(res => {
        console.log(res);
        console.log('BIEN');
        this.usuario = res;
        console.log(res.usua_apell);
      }, err => {
        console.log('::ERROR AL MOMENTO DE CONSULTAR::');
        console.log(err);
      });
  }

  onSelectimagen(event: any) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => { // called once readAsDataURL is completed
        this.url = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]); // read file as data url
    }
  }

  /**
   * Método que permite editar la información del usuario
   */
  goEdit() {
    console.log("Editando");
    this.enform = false;
    this.btnSave = true;
    this.btnEdit = false;
  }

  /**
   * Método que permite guardar la información del usuario
   */
  goSave() {
    this.enform = true;
    this.btnSave = false;
    this.btnEdit = true;
    //Valores por defecto para el objeto Usuario
    this.usuario.usua_tipoiden = 'C';
    this.usuario.usua_password = 'abc$wyw';
    this.usuario.usua_estado = '1';
    this.usuario.usua_genero="M",
    this.usuario.usua_vivienda="P",
    this.usuario.usua_creacion = new Date();
    //Cabecera
    let myHeaders = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    //myHeaders.append('Content-Type', 'application/json; charset=utf-8');
    //myHeaders.set('Content-Type', 'application/json; charset=utf-8');
    var httpOptions = {
      headers: myHeaders
    };
    console.log(this.usuario);
    this.http.post(environment.user_create, this.usuario, httpOptions)
      .subscribe(res => {
        console.log(res);
        console.log('Se ha guardado correctamente');
      }, err => {
        console.log('::ERROR::');
        console.log(err);
      });

  }

  /**
   * Método que indica que se ha cancelado la solicitud
   */
  goCancel() {
    this.enform = true;
    this.btnSave = false;
    this.btnEdit = true;
  }


}
