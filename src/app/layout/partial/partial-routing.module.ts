import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PartialComponent } from './partial.component';

const routes: Routes = [
    {
        path: '', component: PartialComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PartialRoutingModule {
}
