import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { PartialComponent } from './partial.component';
import { PartialRoutingModule } from './partial-routing.module';
import { PageHeaderModule } from './../../shared';

@NgModule({
  imports: [
    CommonModule,
    PartialRoutingModule,
    PageHeaderModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
  ],
  declarations: [PartialComponent]
})
export class PartialModule { }
