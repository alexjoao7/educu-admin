import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

@Component({
  selector: 'app-partial',
  templateUrl: './partial.component.html',
  styleUrls: ['./partial.component.scss'],
  animations: [routerTransition()]
})
export class PartialComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
