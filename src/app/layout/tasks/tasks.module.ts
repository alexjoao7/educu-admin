import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { TasksComponent } from './tasks.component';
import { TasksRoutingModule } from './tasks-routing.module';
import { PageHeaderModule } from './../../shared';


@NgModule({
  imports: [
    CommonModule, TasksRoutingModule, PageHeaderModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
  ],
  declarations: [TasksComponent]
})
export class TasksModule { }
