import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

export interface Tasks {
  titulo: String;
  materia: String;
  enviado: String;
  entregado: String;
  estado: String;
  descripcion: String;
}


@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss'],
  animations: [routerTransition()]
})
export class TasksComponent implements OnInit {

  public task: Tasks[] = [];
  public seletask: Tasks;

  constructor() { }

  ngOnInit() {
    this.getTasksStudent();
  }

  /**
   * Funcion que permite obtener las tareas por estudiante
   */
  getTasksStudent() {
    this.task = [
      {
        titulo: "Ejercicios de matemáticas",
        materia: "MATEMÁTICA",
        enviado: "Dic 02,2018",
        entregado: "Dic 17,2018",
        estado: "P",
        descripcion: "Desarrollar una serie de ejercicios de libro de trabajo"
      },
      {
        titulo: "Ejercicios de matemáticas para el examen",
        materia: "MATEMÁTICA",
        enviado: "Dic 02,2018",
        entregado: "Dic 25,2018",
        estado: "P",
        descripcion: "Estudiar los ejercicios para el exámen final"
      },
      {
        titulo: "Oraciones en tiempo presente",
        materia: "LENGUAJE",
        enviado: "Dic 02,2018",
        entregado: "Dic 18,2018",
        estado: "E",
        descripcion: "Desarrollar 100 oraciones con verbos gramaticales"
      }, {
        titulo: "Ejercicios de física",
        materia: "FISICA",
        enviado: "Dic 02,2018",
        entregado: "Dic 22,2018",
        estado: "E",
        descripcion: "Realizar el experimento de laboratorio del día Lunes"
      }, {
        titulo: "Ejercicios de química",
        materia: "QUÍMICA",
        enviado: "Dic 02,2018",
        entregado: "Dic 10,2018",
        estado: "E",
        descripcion: "Estudiar la tabla períodica y crear los componentes"
      }, {
        titulo: "Ejercicios de matemática",
        materia: "MATEMÁTICA",
        enviado: "Dic 02,2018",
        entregado: "Dic 12,2018",
        estado: "E",
        descripcion: "Desarrollar los ejercicios plateados en clase y estudiar para la prueba."
      }
    ]
  }

  /**
   * Función que permite seleccionar una tarea para visualizar
   * @param task 
   */
  getSelectTask(task: Tasks) {
    this.seletask = task;    
  }

}
