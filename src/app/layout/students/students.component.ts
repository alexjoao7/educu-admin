import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.scss'],
  animations: [routerTransition()]
})
export class StudentsComponent implements OnInit {

  url: String = "";
  prueba: String = "";

  private enform: Boolean = true;
  public btnSave: Boolean = false;
  public btnEdit: Boolean = true;

  countries = [{
    id: 'COU1',
    name: 'Ecuador',
    code: 'EC'
  },
  {
    id: 'COU2',
    name: 'Colombia',
    code: 'CL'
  },
  {
    id: 'COU3',
    name: 'Argentina',
    code: 'AG'
  }];

  bloods = [{
    id: 'o+',
    name: 'O+'
  },
  {
    id: 'o-',
    name: 'O-'
  },
  {
    id: 'a-',
    name: 'A-'
  },
  {
    id: 'a+',
    name: 'A+'
  },
  {
    id: 'ab+',
    name: 'AB+'
  },
  {
    id: 'ab-',
    name: 'AB-'
  }];

  constructor() { }

  ngOnInit() {
    this.url = "assets/images/usuario.png";
  }

  onSelectimagen(event: any) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => { // called once readAsDataURL is completed
        this.url = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]); // read file as data url
    }
  }

  /**
   * Método que permite editar la información del usuario
   */
  goEdit() {
    console.log("Editando");
    this.enform = false;
    this.btnSave = true;
    this.btnEdit = false;
  }

  /**
   * Método que permite guardar la información del usuario
   */
  goSave() {
    this.enform = true;
    this.btnSave = false;
    this.btnEdit = true;
  }

  /**
   * Método que indica que se ha cancelado la solicitud
   */
  goCancel() {
    this.enform = true;
    this.btnSave = false;
    this.btnEdit = true;
  }



}
