import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { StudentsComponent } from './students.component';
import { StudentsRoutingModule } from './students-routing.module';
import { PageHeaderModule } from './../../shared';

@NgModule({
  imports: [
    CommonModule,
    StudentsRoutingModule,
    PageHeaderModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),    
  ],
  declarations: [StudentsComponent]
})
export class StudentsModule { }
