import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditcreateinstitutionRoutingModule } from './editcreateinstitution-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    EditcreateinstitutionRoutingModule
  ]
})
export class EditcreateinstitutionModule { }
