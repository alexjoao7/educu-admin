import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { MessageService } from 'primeng/components/common/api';
import { ActivatedRoute } from "@angular/router";
import { Instituto } from '../../../../models/Instituto';
import { MenuItem } from 'primeng/api';

export interface TypeInstitution {
  id: string,
  name: string
}

export interface TypeEducation {
  id: string,
  name: string
}

export interface Cities {
  id: string,
  name: string
}
@Component({
  selector: 'app-editcreateinstitution',
  templateUrl: './editcreateinstitution.component.html',
  styleUrls: ['./editcreateinstitution.component.scss']
})
export class EditcreateinstitutionComponent implements OnInit {

  id: string;
  instituto: Instituto;
  public imagePath;
  imgURL: any;
  private activeIndex: number = 0;
  private items: MenuItem[];  
  private home: MenuItem;
  private tipe_institution: TypeInstitution[];
  private selectInstitution: TypeInstitution[];
  private tipe_education: TypeEducation[];
  private selectEducation: TypeEducation;
  private cities: Cities[];
  private selectCities: Cities;

  constructor(private location: Location, private messageService: MessageService,
    private route: ActivatedRoute) { }


  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get("id");
    console.log('id: ' + this.id);
    //Componentes del STEPS
    this.home = { icon: 'pi pi-home' };    

    //Para obtener el tipo de educacion
    this.tipe_institution = [
      { id: 'EDIN', name: 'EDUCACIÓN INICIAL' },
      { id: 'EDPR', name: 'EDUCACIÓN PRIMARIA' },
      { id: 'EDSC', name: 'EDUCACION SECUNDARIA' }
    ]

    //Para obtener el tipo de educacion
    this.tipe_education = [
      { id: 'PAR', name: 'PARTICULAR' },
      { id: 'FISC', name: 'FISCAL' },
      { id: 'MUN', name: 'MUNICIPAL' }
    ]

    //Se obtiene las provincias 
    this.cities = [
      { id: 'GYQ', name: 'GUAYAQUIL' },
      { id: 'PCH', name: 'PICHINCHA' },
      { id: 'LJ', name: 'LOJA' },
      { id: 'IMB', name: 'IMBABURA' },
      { id: 'MANB', name: 'MANABI' }
    ]

    this.imgURL = "assets/images/upload.png";

    //Validacion de accion de crear o editar
    if (this.id != null) {
      this.items = [
        { label: 'Instituciones' },
        { label: 'Editar' }
      ];
      this.home = { icon: 'pi pi-home' };
      this.getInstitute();
    } else {
      this.instituto = new Instituto();
      this.items = [
        { label: 'Instituciones' },
        { label: 'Crear instituto' }
      ];
    }

  }

  /**
   * Metodo quqe permite consultar el instituto a editar
   */
  private getInstitute() {
    console.log('CONSULTANDO ??');
    this.instituto = new Instituto();
    this.instituto.id = this.id;
    this.instituto.nombre = "Instituto Uno";
    this.instituto.mision = "Mision uno";
    this.instituto.vision = "Vision uno";
    this.instituto.fundado = "2009";
    this.instituto.telefono = "4758123";
    this.instituto.celular = "09635288474";
    this.instituto.provincia = "PI";
    this.instituto.ciudad = "Quito";
    this.instituto.sector = "Chillogallo";
    this.instituto.direccion = "Santa Rosa de Chillogallo";
    this.instituto.observacion = "Junto a la Cancha del barrio";
    let obj: TypeEducation;
    obj = this.tipe_education.find(obj => obj.id == "PAR");
    this.selectEducation = obj;
    //this.selectInstitution = this.tipe_institution.filter(tipe=> tipe.);
  }

  backinstituto() {
    this.location.back();
  }


  /**
   * Siguiente contador
   */
  next(index: number) {
    this.activeIndex = this.activeIndex + index;
    console.log("index: " + this.activeIndex);
  }

  /**
   * Atras contador
   */
  back(index: number) {
    this.activeIndex = this.activeIndex - index;
    console.log("index: " + this.activeIndex);
  }

  /**
   * Guardar el nuevo cliente
   */
  finalizar() {
    console.log("Finalizando");
    console.log(this.selectInstitution);
  }

  /**
   * Metodo que permite cargar una imagen
   * @param event 
   */
  onUploadImagen(event: any) {
    if (event.files && event.files[0]) {
      var reader = new FileReader();
      this.imagePath = event.files;
      reader.readAsDataURL(event.files[0]);
      reader.onload = (_event) => {
        this.imgURL = reader.result;
      }
      this.messageService.add({ severity: 'info', summary: 'Imagen Uploaded', detail: 'The image has been uploaded' });
    } else {
      this.messageService.add({ severity: 'danger', summary: 'Danger Uploaded', detail: 'The image has not gone up' });
    }

  }

}
