import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InstitutionRoutingModule } from './institution-routing.module';
import { InstitutionComponent } from './institution.component';
import { MessageService } from 'primeng/api';
import { CalendarModule } from 'primeng/calendar';
import { ColorPickerModule } from 'primeng/colorpicker';
import { SpinnerModule } from 'primeng/spinner';
import { ButtonModule } from 'primeng/button';
import { StepsModule } from 'primeng/steps';
import { ToastModule } from 'primeng/toast';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { TableModule } from 'primeng/table';
import { MultiSelectModule } from 'primeng/multiselect';
import { PaginatorModule } from 'primeng/paginator';
import { RadioButtonModule } from 'primeng/radiobutton';
import { SelectButtonModule } from 'primeng/selectbutton';
import { InputSwitchModule } from 'primeng/inputswitch';
import { CheckboxModule } from 'primeng/checkbox';
import { InputTextModule } from 'primeng/inputtext';
import { TriStateCheckboxModule } from 'primeng/tristatecheckbox';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { FileUploadModule } from 'primeng/fileupload';
import { SplitButtonModule } from 'primeng/splitbutton';
import { DialogModule } from 'primeng/dialog';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { SliderModule } from 'primeng/slider';
import { EditcreateinstitutionComponent } from './editcreateinstitution/editcreateinstitution.component';
import { ArchwizardModule } from 'angular-archwizard';

@NgModule({
  imports: [
    CommonModule,
    InstitutionRoutingModule,
    ToastModule,
    CalendarModule,
    ColorPickerModule,
    SpinnerModule,
    ButtonModule,
    StepsModule,
    TableModule,
    MultiSelectModule,
    PaginatorModule,
    RadioButtonModule,
    SelectButtonModule,
    InputSwitchModule,
    CheckboxModule,
    InputTextModule,
    TriStateCheckboxModule,
    InputTextareaModule,
    FileUploadModule,
    SplitButtonModule,
    DialogModule,
    BreadcrumbModule,
    SliderModule,
    ArchwizardModule

  ],
  declarations: [InstitutionComponent, EditcreateinstitutionComponent],
  providers: [MessageService]
})
export class InstitutionModule { }
