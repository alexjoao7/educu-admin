import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InstitutionComponent } from './institution.component';
import { EditcreateinstitutionComponent } from './editcreateinstitution/editcreateinstitution.component';
const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: InstitutionComponent
    },
    { path: 'createinstitute', pathMatch: 'full', component: EditcreateinstitutionComponent },
    { path: 'editinstitute/:id', pathMatch: 'full', component: EditcreateinstitutionComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class InstitutionRoutingModule { }
