import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/components/common/api';
import { Router } from "@angular/router";
import { MenuItem } from 'primeng/api';

export interface Instituto {
  id: string,
  nombre: string,
  mision: string,
  vision: string,
  fundado: string,
  estado: string
}

@Component({
  selector: 'app-institution',
  templateUrl: './institution.component.html',
  styleUrls: ['./institution.component.scss']
})
export class InstitutionComponent implements OnInit {

  private instituto: Instituto[];
  private instituteSelect: Instituto;
  private cols: any[];
  private selectedColumns: any[];
  private display: boolean = false;
  private items: MenuItem[];
  private home: MenuItem;

  constructor(private messageService: MessageService, private router: Router) { }

  ngOnInit() {

    this.instituto = [
      { id: 'ABC1', nombre: "Colegio uno", mision: "Mision 1", vision: "Vision 1", fundado: "1995", estado: "1" },
      { id: 'ABC2', nombre: "Colegio dos", mision: "Mision 2", vision: "Vison 2", fundado: "2002", estado: "1" },
      { id: 'ABC3', nombre: "Colegio tres", mision: "Mision 3", vision: "Vision 3", fundado: "1990", estado: "1" },
    ]
    this.cols = [
      { field: 'nombre', header: 'Nombre' },
      { field: 'mision', header: 'Mision' },
      { field: 'vision', header: 'Vision' },
      { field: 'fundado', header: 'Año Fundación' },
      { field: 'estado', header: 'Estado' }
    ];
    this.selectedColumns = this.cols;

    this.items = [
      { label: 'Instituciones' },
      //{ label: 'Lionel Messi', url: 'https://en.wikipedia.org/wiki/Lionel_Messi' }
    ];

    this.home = { icon: 'pi pi-home' };
  }

  /**
  * Metodo que permite eliminar un objeto
  * */
  private delete() {
    this.display = true;
  }

  /**
   * Metodo que permite seleccionar un elemento de la tabla
   */
  private isSelect() {
    return (this.instituteSelect == null)
  }

}
