import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PropertiesComponent } from '../properties/properties.component';
import { EditcreatepropertiesComponent } from '../properties/editcreateproperties/editcreateproperties.component';

const routes: Routes = [
  {
    path: '',
    component: PropertiesComponent
  },
  { path: 'createproperties', pathMatch: 'full', component: EditcreatepropertiesComponent },
  { path: 'editproperties/:id', pathMatch: 'full', component: EditcreatepropertiesComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PropertiesRoutingModule { }
