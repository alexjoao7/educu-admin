import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
export interface Properties {
  id: string;
  nombre: string;
  valor: string;
  observacion: string;
  estado: string;
}
@Component({
  selector: 'app-properties',
  templateUrl: './properties.component.html',
  styleUrls: ['./properties.component.scss']
})
export class PropertiesComponent implements OnInit {

  private items: MenuItem[];
  private home: MenuItem;
  private properties: Properties[];
  private propertiesSelect: Properties;
  private cols: any[];
  private selectedColumns: any;

  constructor() { }

  ngOnInit() {
    this.items = [
      { label: 'Propiedades' },
    ];
    this.properties = [
      { id: "PR1", nombre: "Provincia", valor: "Pichincha", observacion: "Dato uno", estado: "1" },
      { id: "PR2", nombre: "Provincia", valor: "Loja", observacion: "Dato dos", estado: "1" },
      { id: "PR3", nombre: "Institución", valor: "Pública", observacion: "Dato tres", estado: "1" },
      { id: "PR4", nombre: "Institución", valor: "Privada", observacion: "Dato cuatro", estado: "1" },
    ]

    this.cols = [
      { field: 'nombre', header: 'Nombre' },
      { field: 'valor', header: 'Valor' },
      { field: 'observacion', header: 'Observación' },
      { field: 'estado', header: 'Estado' }
    ];
    this.selectedColumns = this.cols;

    this.home = { icon: 'pi pi-home' };
  }

  /**
   * Metodo que permite seleccionar un elemento de la tabla
   */
  private isSelect() {
    return (this.propertiesSelect == null)
  }

}
