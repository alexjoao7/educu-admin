import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditcreatepropertiesComponent } from './editcreateproperties.component';
import { StepsModule } from 'primeng/steps';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { MultiSelectModule } from 'primeng/multiselect';
import { KeyFilterModule } from 'primeng/keyfilter';
import { CalendarModule } from 'primeng/calendar';
import { RadioButtonModule } from 'primeng/radiobutton';
import { DropdownModule } from 'primeng/dropdown';
import { SelectButtonModule } from 'primeng/selectbutton';
@NgModule({
  declarations: [EditcreatepropertiesComponent],
  imports: [
    CommonModule,
    StepsModule,
    ButtonModule,
    InputTextModule,
    MultiSelectModule,
    KeyFilterModule,
    CalendarModule,
    RadioButtonModule,
    DropdownModule,
    SelectButtonModule
  ]
})
export class EditcreatepropertiesModule { }
