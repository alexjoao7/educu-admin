import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { MenuItem } from 'primeng/api';
import { ActivatedRoute } from "@angular/router";
import { MessageService } from 'primeng/components/common/api';
import { Propiedades } from '../../../../models/Propiedades';

@Component({
  selector: 'app-editcreateproperties',
  templateUrl: './editcreateproperties.component.html',
  styleUrls: ['./editcreateproperties.component.scss']
})
export class EditcreatepropertiesComponent implements OnInit {

  id: string;
  items: MenuItem[];
  home: MenuItem;
  properties: Propiedades;

  constructor(private location: Location, private messageService: MessageService, private route: ActivatedRoute) { }

  ngOnInit() {

    this.id = this.route.snapshot.paramMap.get("id");
    console.log('id estudiante : ' + this.id);
    //Validacion de accion de crear o editar
    if (this.id != null) {
      this.items = [
        { label: 'Propiedades' },
        { label: 'Editar' }
      ];
      this.getProperties();
    } else {
      this.properties = new Propiedades();
      this.items = [
        { label: 'Propiedades' },
        { label: 'Crear propiedad' }
      ];
    }
    this.home = { icon: 'pi pi-home' };
  }

  getProperties() {
    console.log('Obteniendo las propiedades');
  }

  back() {
    this.location.back();
  }
}
