import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PropertiesComponent } from '../properties/properties.component';
import { PropertiesRoutingModule } from './properties-routing.module';
import { MessageService } from 'primeng/api';
//Componentes
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { TableModule } from 'primeng/table';
import { MultiSelectModule } from 'primeng/multiselect';
import { PaginatorModule } from 'primeng/paginator';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { ToastModule } from 'primeng/toast';
import { EditcreatepropertiesComponent } from './editcreateproperties/editcreateproperties.component';

@NgModule({
  declarations: [PropertiesComponent, EditcreatepropertiesComponent],
  imports: [
    CommonModule,
    PropertiesRoutingModule,
    BreadcrumbModule,
    TableModule,
    MultiSelectModule,
    PaginatorModule,
    ButtonModule,
    InputTextModule,
    ToastModule
  ],
  providers: [MessageService],
})
export class PropertiesModule { }
