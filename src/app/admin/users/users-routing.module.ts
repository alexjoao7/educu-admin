import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users.component';
import { EditcreateuserComponent } from './editcreateuser/editcreateuser.component';

const routes: Routes = [
    {
        path: '',
        component: UsersComponent
    },
    { path: 'createuser', pathMatch: 'full', component: EditcreateuserComponent },
    { path: 'edituser/:id', pathMatch: 'full', component: EditcreateuserComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsersRoutingModule { }
