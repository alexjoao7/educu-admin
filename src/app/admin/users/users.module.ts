import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessageService } from 'primeng/api';
import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { FormsModule } from '@angular/forms';
import { EditcreateuserComponent } from './editcreateuser/editcreateuser.component';
//Componentes a utilizar
import { CalendarModule } from 'primeng/calendar';
import { ColorPickerModule } from 'primeng/colorpicker';
import { SpinnerModule } from 'primeng/spinner';
import { ButtonModule } from 'primeng/button';
import { StepsModule } from 'primeng/steps';
import { ToastModule } from 'primeng/toast';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { TableModule } from 'primeng/table';
import { MultiSelectModule } from 'primeng/multiselect';
import { PaginatorModule } from 'primeng/paginator';
import { RadioButtonModule } from 'primeng/radiobutton';
import { SelectButtonModule } from 'primeng/selectbutton';
import { InputSwitchModule } from 'primeng/inputswitch';
import { CheckboxModule } from 'primeng/checkbox';
import { InputTextModule } from 'primeng/inputtext';
import { TriStateCheckboxModule } from 'primeng/tristatecheckbox';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { ArchwizardModule } from 'angular-archwizard';

@NgModule({
  imports: [
    CommonModule,
    UsersRoutingModule,
    FormsModule,
    CalendarModule,
    ColorPickerModule,
    SpinnerModule,
    ButtonModule,
    StepsModule,
    ToastModule,
    AutoCompleteModule,
    TableModule,
    MultiSelectModule, 
    PaginatorModule, 
    RadioButtonModule,
    SelectButtonModule,
    InputSwitchModule,
    CheckboxModule,
    InputTextModule,
    TriStateCheckboxModule,
    BreadcrumbModule,
    ArchwizardModule
  ],
  declarations: [UsersComponent, EditcreateuserComponent],
  providers: [MessageService],
})
export class UsersModule { }
