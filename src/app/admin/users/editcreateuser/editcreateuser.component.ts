import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from "@angular/router";
import { MenuItem } from 'primeng/api';
import { MessageService } from 'primeng/components/common/api';
import { Usuario } from '../../../../models/Usuario';

export interface TypeIdentity {
  name: string,
  value: string
}

export interface Perfiles {
  name: string,
  value: string
}

@Component({
  selector: 'app-editcreateuser',
  providers: [MessageService],
  templateUrl: './editcreateuser.component.html',
  styleUrls: ['./editcreateuser.component.scss']
})
export class EditcreateuserComponent implements OnInit {

  id: string;
  items: MenuItem[];
  home: MenuItem;
  tipeidentity: TypeIdentity[];
  perfiles: Perfiles[];
  gender: string;
  birth_date: Date;
  usuario: Usuario;

  constructor(private location: Location, private messageService: MessageService, private route: ActivatedRoute) { }

  ngOnInit() {

    this.id = this.route.snapshot.paramMap.get("id");
    console.log('id estudiante : ' + this.id);

    this.tipeidentity = [
      { name: 'CEDULA', value: 'CI' },
      { name: 'RUC', value: 'RUC' },
      { name: 'PASAPORTE', value: 'P' }
    ]

    this.perfiles = [
      { name: "ADMINISTRADOR", value: "ADMIN" },
      { name: "DOCENTE", value: "DOC" },
      { name: "PADRE DE FAMILIA", value: "PF" },
      { name: "ESTUDIANTE", value: "EST" },
      { name: "SUPER ADMINISTRADOR", value: "SUPER" }
    ]

    //Validacion de accion de crear o editar
    if (this.id != null) {
      this.items = [
        { label: 'Usuarios' },
        { label: 'Editar' }
      ];
      this.getUsuarios();
    } else {
      this.usuario = new Usuario();
      this.items = [
        { label: 'Usuarios' },
        { label: 'Crear usuario' }
      ];
    }
    this.home = { icon: 'pi pi-home' };

  }

  getUsuarios() {
    console.log('Obteniendo los usuarios creados');
  }

  /**
   * Guardar el nuevo cliente
   */
  finalizar() {
    console.log("Finalizando");
  }

  backusers() {
    this.location.back();
  }

}  
