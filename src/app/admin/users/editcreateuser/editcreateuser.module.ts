import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessageService } from 'primeng/api';
import { EditcreateuserComponent } from './editcreateuser.component';
import { EditcreateuserRoutingModule } from './editcreateuser-routing.module';
import { StepsModule } from 'primeng/steps';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { MultiSelectModule } from 'primeng/multiselect';
import { KeyFilterModule } from 'primeng/keyfilter';
import { CalendarModule } from 'primeng/calendar';
import { RadioButtonModule } from 'primeng/radiobutton';
import { DropdownModule } from 'primeng/dropdown';
import { SelectButtonModule } from 'primeng/selectbutton';
import { ArchwizardModule } from 'angular-archwizard';

@NgModule({
  imports: [
    CommonModule,
    EditcreateuserRoutingModule,
    StepsModule,
    ButtonModule,
    InputTextModule,
    MultiSelectModule,
    KeyFilterModule,
    CalendarModule,
    RadioButtonModule,
    DropdownModule,
    SelectButtonModule,
    ArchwizardModule
  ],
  declarations: [EditcreateuserComponent],
  providers: [MessageService]
})
export class EditcreateuserModule { }
