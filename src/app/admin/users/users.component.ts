import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/components/common/api';
import { MenuItem } from 'primeng/api';

export interface Usuario {
  nombre: string,
  email: string,
  perfiles: string,
  estado: string
}

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  private users: Usuario[];
  private userSelect: Usuario;
  private cols: any[];
  private selectedColumns: any[];
  private items: MenuItem[];
  private home: MenuItem;

  constructor(private messageService: MessageService) {
  }

  ngOnInit() {

    this.users = [
      { nombre: "Alex", email: "alex@gmail.com", perfiles: "ADMIN", estado: "1" },
      { nombre: "Martina", email: "martina@gmail.com", perfiles: "ESTUDIANTE", estado: "1" },
      { nombre: "Emilia", email: "emilia@gmail.com", perfiles: "ESTUDIANTE", estado: "0" },
    ]

    this.cols = [
      { field: 'nombre', header: 'Nombre' },
      { field: 'email', header: 'Email' },
      { field: 'perfiles', header: 'Perfiles' },
      { field: 'estado', header: 'Estado' }
    ];
    this.selectedColumns = this.cols;

    this.items = [
      { label: 'Usuarios' },
      //{ label: 'Lionel Messi', url: 'https://en.wikipedia.org/wiki/Lionel_Messi' }
    ];

    this.home = { icon: 'pi pi-home' };

  }

  delete() {
    console.log('ELIMINAR');
  }

  onSubmit() {
    // do something here
  }

  /**
   * Metodo que permite seleccionar un elemento de la tabla
   */
  private isSelect() {
    return (this.userSelect == null)
  }



}
